# OLIP base image for Raspberry pi

This repository was forked from
[offspot/base-image](https://github.com/offspot/base-image) -
where the real work still happens.
It was modified in order to be used on BSF's OLIP deployments (see 1d60bf7 and following).

## Usage

Build the image:

```sh
build-docker.sh
```

## Configuration

Example `config` file:

```sh
IMG_NAME=ideascube
STAGE_LIST="stage0 stage1 stage2"
FIRST_USER_NAME="ideascube"
FIRST_USER_PASS="ideascube"
ENABLE_SSH=1
PUBKEY_SSH_FIRST_USER=""
TIMEZONE_DEFAULT="Europe/Paris"
TARGET_HOSTNAME="ideascube"
DEPLOY_ZIP="1"
USE_QCOW2="0"
```

More information at <https://github.com/RPi-Distro/pi-gen#config>

Download image at <https://drop.bsf-intranet.org/image_2022-05-19-ideascube-lite.zip>
